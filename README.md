# Sveltekit Lucia - Auth - Shadcn
###
  | Name | Library |
  | --------- | --------------------------- |
  | Framework | SvelteKit |
  | CSS | Tailwind CSS, Shadcn-svelte |
  | Database | Supabase |

## Pages

- Sign Up
- Log In Page
- Home Page

