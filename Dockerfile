FROM node:18-alpine

WORKDIR /app

#COPY rollup.config.js ./
COPY package*.json ./

RUN npm install

COPY . ./

RUN npm install

EXPOSE 5173

ENV HOST=0.0.0.0

CMD [ "npm", "run", "dev" ]

